/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50540
 Source Host           : localhost:3306
 Source Schema         : shopping

 Target Server Type    : MySQL
 Target Server Version : 50540
 File Encoding         : 65001

 Date: 19/12/2021 13:19:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `adminname` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `adminpassword` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`adminname`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('admin', 'admin');

-- ----------------------------
-- Table structure for cartitem
-- ----------------------------
DROP TABLE IF EXISTS `cartitem`;
CREATE TABLE `cartitem`  (
  `cartItemId` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `count` int(11) NULL DEFAULT NULL,
  `pid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `uid` int(11) NULL DEFAULT NULL,
  `orderBy` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`cartItemId`) USING BTREE,
  INDEX `orderBy`(`orderBy`) USING BTREE,
  INDEX `FK_t_cartitem_t_user`(`uid`) USING BTREE,
  INDEX `FK_t_cartitem_t_book`(`pid`) USING BTREE,
  CONSTRAINT `FK_t_cartitem_t_book` FOREIGN KEY (`pid`) REFERENCES `goods` (`pid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_t_cartitem_t_user` FOREIGN KEY (`uid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 57 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cartitem
-- ----------------------------
INSERT INTO `cartitem` VALUES ('496B7D81DF474254A43E33F01BDF04DF', 1, 'b70401211a764ea284e1ec16394bc56a', 3, 56);
INSERT INTO `cartitem` VALUES ('6393F901EBC64C348EC6BF8B6249F6A8', 6, '0e44471806b248feadd2a3b21d6c6a81', 3, 55);

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `cid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cname` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cid`) USING BTREE,
  UNIQUE INDEX `cname`(`cname`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('6', '厨房用品');
INSERT INTO `category` VALUES ('2', '品牌手机');
INSERT INTO `category` VALUES ('3', '品牌电脑');
INSERT INTO `category` VALUES ('1', '家用电器');
INSERT INTO `category` VALUES ('4', '户外骑行');
INSERT INTO `category` VALUES ('5', '智能商品');
INSERT INTO `category` VALUES ('8', '服装百货');
INSERT INTO `category` VALUES ('7', '速食美味');

-- ----------------------------
-- Table structure for face_info
-- ----------------------------
DROP TABLE IF EXISTS `face_info`;
CREATE TABLE `face_info`  (
  `face_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '人脸id',
  `face_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人脸姓名',
  `face_post` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人脸职位',
  `face_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人脸照片',
  `face_age` int(11) NOT NULL COMMENT '人脸年龄',
  `face_sex` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人脸性别',
  PRIMARY KEY (`face_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of face_info
-- ----------------------------


-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `pid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pname` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `market_price` double NULL DEFAULT NULL,
  `good_price` double NULL DEFAULT NULL,
  `pimages` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pdate` date NULL DEFAULT NULL,
  `is_hot` int(10) NULL DEFAULT NULL,
  `pflag` int(10) NULL DEFAULT NULL,
  `pdesc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`pid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES ('0e44471806b248feadd2a3b21d6c6a81', '好吃', 998, 520, '/C/A/8FC9CC08FF274FCBADECF01E6C8121CD.jfif', '2021-06-20', 1, 0, 'jsu', '7');
INSERT INTO `goods` VALUES ('1', '小米加湿器', 99, 88, 'jiadian_xiaomi_001.jpg', '2020-12-23', 0, 0, '小米米家加湿器4L家用静音卧室大雾量办公孕妇婴儿抗菌杀菌官方', '1');
INSERT INTO `goods` VALUES ('10', '康师傅', 15, 14.8, 'eat_kangsf_001.jpg', '2020-12-27', 1, 0, '【夕夕超市】 康师傅方便面大食袋红烧牛肉面袋面泡面五连包休闲零食 ', '7');
INSERT INTO `goods` VALUES ('11', '七匹狼T恤', 559, 119, 'cloth_7pl_001.jpg', '2020-12-28', 1, 0, 'Q1七匹狼长袖T恤翻领男士新品商务休闲条纹polo衫保罗衫男装春秋装男体恤衫 101(藏青) 175/92A/XL ', '8');
INSERT INTO `goods` VALUES ('12', '牛皮鞋', 5800, 3400, 'cloth_tods_001.jpg', '2020-12-28', 0, 0, 'TOD\'S 男士牛皮系带鞋 礼盒礼品 棕色 42 ', '8');
INSERT INTO `goods` VALUES ('13', 'balabala童装', 299, 199, 'cloth_balabala_001.jpg', '2020-12-28', 0, 0, '巴拉巴拉棉服连体衣婴儿衣服宝宝冬装外出抱衣加厚爬爬服保暖动物 杏黄3510 90cm ', '8');
INSERT INTO `goods` VALUES ('14', '宿舍锅', 89.9, 79.9, 'cooker_ten_001.jpg', '2020-12-28', 1, 0, 'Tenscare医而维多功能电煮锅电火锅电炒锅电蒸锅电热锅学生寝室宿舍小锅家用火锅泡面煮面锅1.5L 机械两档款（不带蒸笼） ', '6');
INSERT INTO `goods` VALUES ('15', '火鸡面', 30, 28.99, 'eat_sanyang_002.jpg', '2020-12-28', 1, 0, ' 韩国进口（SAMYANG） 三养辣鸡肉味拌面 700g（140g*5包入）超辣火鸡面方便面袋面速食零食品 ', '7');
INSERT INTO `goods` VALUES ('16', '拯救者', 7899, 7199, 'computer_lenovo_001.jpg', '2021-01-01', 1, 0, '联想(Lenovo)拯救者Y7000P 15.6英寸游戏笔记本电脑(i7-10750H 16G 512G SSD GTX1650Ti 144Hz100%sRGB)', '3');
INSERT INTO `goods` VALUES ('17', '沙芬烟斗', 1298, 1233, 'index_2.jpg', '2021-01-01', 0, 0, '沙芬SAVINELLI 苔原年轮纹石楠木烟斗 意大利进口男士烟斗烟具 进口烟丝专用 男士礼物 苔原645', '10');
INSERT INTO `goods` VALUES ('18', '咖啡机', 888, 866, 'index_003.jpg', '2021-01-01', 0, 0, '	NESPRESSO Inissia 胶囊咖啡机进口小型迷你办公家用全自动咖啡机', '11');
INSERT INTO `goods` VALUES ('2', '小米手机K30pro', 2000, 1999, 'phone_xiaomi_001.jpg', '2020-12-23', 1, 0, 'Xiaomi/小米10青春版5G手机全网通官方旗舰店官网小米手机青春10x红米K30pro10Xpro', '2');
INSERT INTO `goods` VALUES ('20', '2', 3, 4, '5', '2020-12-23', 7, 8, '9', '0');
INSERT INTO `goods` VALUES ('22', '华为手表', 798, 666, 'huawei_shoubiao.jpg', '2021-01-21', 1, 0, '华为手表智能手表 儿童电话手表 3Pro粉 定位手表 4G全网通/视频通话/九重定位/小度助手 学生男孩女孩 ', '5');
INSERT INTO `goods` VALUES ('23', '紫砂壶', 199, 99, '/C/6/443A05FF87F24CE0BFF78BD785ED2DB6.jpg', '2021-01-22', 1, 0, '倒把西施 宜兴紫砂壶名家纯全手工西施壶原矿紫泥泡茶壶球孔功夫茶具套装 卡盖大西施(360ml) ', '6');
INSERT INTO `goods` VALUES ('231270c8f244456984c5c161acfa3abc', '有手就行', 521, 520, '/A/E/79D4A6823AEE42FA83231E97953BC662.jpg', '2021-05-21', 1, 0, '举手之劳', '2');
INSERT INTO `goods` VALUES ('24', 'ppl', 123, 123, '/B/0/AFB4D581598448AC9295EDDE9716FE05.jpg', '2021-04-21', 1, 0, '123', '6');
INSERT INTO `goods` VALUES ('3', '华为手机Mate 400', 4666, 8888, 'phone_huawei_001.jpg', '2020-12-22', 1, 0, '24期免息/现货速发】Huawei/华为nova7 Pro 5G手机官方旗舰店nova8正品p40新款p30直降荣耀30保时捷mate40pro', '2');
INSERT INTO `goods` VALUES ('3ebcc34cad104d249148ff270c3acde0', '小米旗舰', 2000000, 1999999, '/8/6/0610466C2D6D4EC6B47D34D6CBB2B8FF.png', '2021-05-21', 1, 0, '小米旗舰logo 日本大师两年心血之作', '2');
INSERT INTO `goods` VALUES ('4', '小米电脑', 5200, 4699, 'computer_xiaomi_001.jpg', '2020-12-21', 1, 0, '【24期免息】小米redmibook14十代i5轻薄便携商务办公MX350独显学生笔记本电脑14英寸苏宁易购官方正品', '3');
INSERT INTO `goods` VALUES ('5', '华为Mate40Pro', 9999, 7849, 'phone_huawei_002.jpg', '2020-12-27', 1, 0, '华为 Mate40 Pro 5G手机88度曲面屏，50倍变焦前二后四摄。麒麟9000，sos芯片，运行速度快，性能稳定。', '2');
INSERT INTO `goods` VALUES ('6', '小米10', 5399, 4399, 'phone_xiaomi_003.jpg', '2020-12-27', 1, 0, '【现货速发】小米10 双模5G 骁龙865 1亿像素8K电影相机 对称式立体声 小米手机 冰海蓝', '2');
INSERT INTO `goods` VALUES ('6f4899a54cd04d43a2abb31e306ea795', '星空梦想', 10086, 12315, '/2/C/2CFBBBA908AC4C97B3326541F7132DC4.gif', '2021-05-21', 1, 0, '鲁班七号同款限定', '5');
INSERT INTO `goods` VALUES ('7', 'DAHON大行', 4750, 3278, 'bicycle_DAHON_001.jpg', '2020-12-27', 1, 0, 'DAHON大行P8折叠自行车20英寸8速成人男女休闲单车经典款KBC083 橙色', '4');
INSERT INTO `goods` VALUES ('7067a9928ad64755b05a1e02baede542', '大西瓜', 123, 123, '/D/8/BEEA3B34F43645B580FD8CD1E42A0ECC.png', '2021-06-16', 1, 0, '大西瓜', '7');
INSERT INTO `goods` VALUES ('8', '小米手环5', 220, 199, 'bracelect_xiaomi_001.jpg', '2020-12-27', 1, 0, '小米手环 5 石墨黑 动态彩屏 心率运动手环50米防水女性健康24小时高精准心率监测', '5');
INSERT INTO `goods` VALUES ('8d63cd76b5f245378484cbee95cb5386', '御姐·皮衣', 123, 9999999.8, '/F/9/550EA433B47846FAB33B6C5AE1E9A0FF.jpeg', '2021-10-05', 1, 0, 'dfgg ', '8');
INSERT INTO `goods` VALUES ('9', '高压锅', 200, 196, 'cooder_qld_001.jpg', '2020-12-27', 1, 0, '乾莱达 双喜高压锅家用燃气电磁炉通用小迷你防爆大压力锅1-2-3-4-5-6人 16cm/明火电磁炉通用/ 2.2升容量适用1-2 ', '6');
INSERT INTO `goods` VALUES ('b70401211a764ea284e1ec16394bc56a', '猫和老鼠', 1.9, 0.9, '/A/1/7150CC9D68D445E491FE1FC5CB6C2957.gif', '2021-05-21', 1, 0, '世纪大电影', '4');
INSERT INTO `goods` VALUES ('bb01b60c2aaf4d5da8d118f32997c381', '奥利给', 123, 123, '/A/3/35D32215546F4812883E720F9CB758E7.jpg', '2021-06-26', 1, 0, '123123', '8');
INSERT INTO `goods` VALUES ('ce981e410a6848c6bde9a48db9fa536f', '周杨', 123, 123, '/6/E/49234EF1FF2743CF953B4BA069D871F1.jpg', '2021-06-26', 1, 0, '123', '7');

-- ----------------------------
-- Table structure for orderitem
-- ----------------------------
DROP TABLE IF EXISTS `orderitem`;
CREATE TABLE `orderitem`  (
  `itemid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `count` int(11) NULL DEFAULT NULL,
  `subtotal` double NULL DEFAULT NULL,
  `pid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `oid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`itemid`) USING BTREE,
  INDEX `fk_0001`(`pid`) USING BTREE,
  INDEX `fk_0002`(`oid`) USING BTREE,
  CONSTRAINT `fk_0001` FOREIGN KEY (`pid`) REFERENCES `goods` (`pid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_0002` FOREIGN KEY (`oid`) REFERENCES `orders` (`oid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of orderitem
-- ----------------------------
INSERT INTO `orderitem` VALUES ('08230F73B8624D15B3EFF49FC401782C', 1, 88, '1', '919505A586CB4922A92A48905D64AC46');
INSERT INTO `orderitem` VALUES ('1826150FC0A44B97BE65AF2DFA6892AD', 1, 119, '11', 'B9F8BA68592042DAA26C0D6F7F7C327A');
INSERT INTO `orderitem` VALUES ('1C5FB570F0DF48E78A1A408435A5C141', 1, 99, '23', '3860FBA8C0BC4AC99BD03370BB23D8C5');
INSERT INTO `orderitem` VALUES ('1CC238B009F848AD8C6953455003B357', 1, 0.9, 'b70401211a764ea284e1ec16394bc56a', 'CA0B084F558D41FEA2C6E3646757F563');
INSERT INTO `orderitem` VALUES ('1D13E7A0D4B64641B7249B2A06DFC174', 2, 398, '8', '4C41C962EAAA44B8AE6BCF8B23034016');
INSERT INTO `orderitem` VALUES ('2ba0845cf5e04286864d7d32832533f1', 2, 9398, '4', '3a2927fe73ef4521ad7eaea756d119e6');
INSERT INTO `orderitem` VALUES ('2DD8369CDEDD4CF5BF6EFA787C46D034', 2, 398, '8', 'DAB6787E46DA467DAAD1BE3B996F4DDF');
INSERT INTO `orderitem` VALUES ('2dd94be9153342f69e5ff15cc275d402', 1, 4699, '4', '5ecf6c3bb1e54f72922cfbde598f82ec');
INSERT INTO `orderitem` VALUES ('32A3836B1CF049299A4BF9A4B71C8DA9', 1, 0.9, 'b70401211a764ea284e1ec16394bc56a', '5F5F3C548037453CA6165F60DF339888');
INSERT INTO `orderitem` VALUES ('37E0722AA7F9434F886E704181814350', 1, 99, '23', 'CA0B084F558D41FEA2C6E3646757F563');
INSERT INTO `orderitem` VALUES ('39b3b05ef7bd4055b692620d979fd0a9', 2, 1332, '22', '5ecf6c3bb1e54f72922cfbde598f82ec');
INSERT INTO `orderitem` VALUES ('3c88ce55b9c04953870bbc15b507b849', 3, 13197, '6', '08f79a9045ee4efea321e564c46516a7');
INSERT INTO `orderitem` VALUES ('3ff285e4bfd040bfbe14d1d97927421b', 1, 28.99, '15', '5ecf6c3bb1e54f72922cfbde598f82ec');
INSERT INTO `orderitem` VALUES ('4A305569CC744145917A394EDA1B697C', 1, 7199, '16', '2326A8AF156A4278B8F489C0682FF035');
INSERT INTO `orderitem` VALUES ('5649C8CFE4E948D6A8D7556840D04041', 1, 12315, '6f4899a54cd04d43a2abb31e306ea795', '3860FBA8C0BC4AC99BD03370BB23D8C5');
INSERT INTO `orderitem` VALUES ('62CC457CCB8A4D299978A3087208AAE8', 2, 398, '8', '2326A8AF156A4278B8F489C0682FF035');
INSERT INTO `orderitem` VALUES ('659EC105BD12440F9472B15D79E81480', 1, 7199, '16', 'CA0B084F558D41FEA2C6E3646757F563');
INSERT INTO `orderitem` VALUES ('7027d42af0d34b88bb2edaf3c251b229', 1, 28.99, '15', '5b16dbf6675f4ec9a6b2d0b748c42c22');
INSERT INTO `orderitem` VALUES ('7728516594A643F08CCC4604420A695A', 1, 122, '7067a9928ad64755b05a1e02baede542', 'DAB6787E46DA467DAAD1BE3B996F4DDF');
INSERT INTO `orderitem` VALUES ('87B44C721C324D0AA09700B46A960990', 1, 7199, '16', '16F0DE3705E645B19A8D97E7BD4E8E8D');
INSERT INTO `orderitem` VALUES ('8DB57571597D4BAFBBDCD5FF6E31DCC8', 1, 1999999, '3ebcc34cad104d249148ff270c3acde0', 'B9F8BA68592042DAA26C0D6F7F7C327A');
INSERT INTO `orderitem` VALUES ('90e0ea27256e4849a5aeb18bc958a0ac', 1, 666, '22', '980a5162b7e046548978d2baa4bfbe7b');
INSERT INTO `orderitem` VALUES ('ab7e61f99c1b4541bc46bc271466a9ce', 1, 666, '22', 'd29393fa5def450fb0831c2a489ffa71');
INSERT INTO `orderitem` VALUES ('B7A961126E0C4B9088546D00DDCBCC86', 1, 119, '11', '137574B69ADA45278713A30C66DC7CA0');
INSERT INTO `orderitem` VALUES ('c19473475b5f43d08cdacdfda4250b7b', 1, 199, '8', 'e4b6d5c56dcf454996220ec94e0546df');
INSERT INTO `orderitem` VALUES ('c8c2afa3b1ba440e84393555c1d904f7', 2, 57.98, '15', '3a2927fe73ef4521ad7eaea756d119e6');
INSERT INTO `orderitem` VALUES ('D83D879CE367409D93FCF8B9163D40B8', 1, 99, '23', 'B6638893A61149558804602FD13CF8D2');
INSERT INTO `orderitem` VALUES ('e320049cf9554cca87871b4e8aaedd7d', 1, 7849, '5', '58436a8c3ad245da80cb0ec9046971c5');
INSERT INTO `orderitem` VALUES ('e49a1509196d41aaa3750bd08d8e2dcd', 1, 4699, '4', 'a96d31d3e42e47aeb6a7851ba3b9cc05');
INSERT INTO `orderitem` VALUES ('EFD8595E289D43B9BACD3E076DEB2D90', 1, 99, '23', 'B9F8BA68592042DAA26C0D6F7F7C327A');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `oid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ordertime` datetime NULL DEFAULT NULL,
  `total` double NULL DEFAULT NULL,
  `state` int(11) NULL DEFAULT NULL,
  `address` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `telephone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `uid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('08f79a9045ee4efea321e564c46516a7', '2021-03-30 20:20:49', 13197, 2, '吉首大学', '张三', '13326548754', '3');
INSERT INTO `orders` VALUES ('137574B69ADA45278713A30C66DC7CA0', '2021-06-16 23:15:07', 119, 5, '吉首大学火焰山', '龚佳龙', '13367377672', '3');
INSERT INTO `orders` VALUES ('16F0DE3705E645B19A8D97E7BD4E8E8D', '2021-06-16 21:15:43', 7199, 0, '吉首大学火焰山', '龚佳龙', '龚佳龙', '3');
INSERT INTO `orders` VALUES ('2326A8AF156A4278B8F489C0682FF035', '2021-06-16 23:14:27', 7597, 0, '吉首大学火焰山', '龚佳龙', '13367377672', '3');
INSERT INTO `orders` VALUES ('3860FBA8C0BC4AC99BD03370BB23D8C5', '2021-06-15 18:14:13', 12315, 3, '启航实验室aasdasda', '波波', '波波', '3');
INSERT INTO `orders` VALUES ('3a2927fe73ef4521ad7eaea756d119e6', '2021-01-24 14:13:35', 9455.98, 3, '启航实验室', 'ppl', '13323222322', '3');
INSERT INTO `orders` VALUES ('4C41C962EAAA44B8AE6BCF8B23034016', '2021-06-20 16:43:20', 398, 0, '', '', '', '3');
INSERT INTO `orders` VALUES ('58436a8c3ad245da80cb0ec9046971c5', '2021-01-23 17:56:49', 7849, 1, '启航实验室', '波波', '111111111111', '3');
INSERT INTO `orders` VALUES ('5b16dbf6675f4ec9a6b2d0b748c42c22', '2021-01-14 20:14:39', 28.99, 3, '启航实验室', '波波', '111111111111', '3');
INSERT INTO `orders` VALUES ('5ecf6c3bb1e54f72922cfbde598f82ec', '2021-02-12 23:33:02', 6059.99, 3, '爱心幼儿园', '琪琪拖', '123456789', '3');
INSERT INTO `orders` VALUES ('5F5F3C548037453CA6165F60DF339888', '2021-06-16 21:16:39', 0.9, 0, '吉首大学火焰山2.0', '龚佳龙', '龚佳龙', '3');
INSERT INTO `orders` VALUES ('919505A586CB4922A92A48905D64AC46', '2021-06-15 18:10:15', 88, 0, '启航实验室', '龚佳龙', '龚佳龙', '3');
INSERT INTO `orders` VALUES ('980a5162b7e046548978d2baa4bfbe7b', '2021-01-23 19:18:39', 666, 3, '吉首大学', '马冬梅', '13323222322', '3');
INSERT INTO `orders` VALUES ('a96d31d3e42e47aeb6a7851ba3b9cc05', '2021-01-22 14:33:58', 4699, 3, '启航实验室', '波波', '111111111111', '3');
INSERT INTO `orders` VALUES ('B6638893A61149558804602FD13CF8D2', '2021-06-20 15:08:49', 99, 0, '吉首大学火焰山', '龚佳龙', '13326548754', '3');
INSERT INTO `orders` VALUES ('B9F8BA68592042DAA26C0D6F7F7C327A', '2021-06-15 21:22:04', 2000217, 5, '启航实验室', '波波', '波波', '3');
INSERT INTO `orders` VALUES ('CA0B084F558D41FEA2C6E3646757F563', '2021-06-15 17:27:50', 7298.9, 0, '吉首大学火焰山', 'PPL', '123123123', '3');
INSERT INTO `orders` VALUES ('d29393fa5def450fb0831c2a489ffa71', '2021-01-23 19:17:47', 666, 1, '启航实验室', '波波', '111111111111', '3');
INSERT INTO `orders` VALUES ('DAB6787E46DA467DAAD1BE3B996F4DDF', '2021-06-20 16:42:36', 520, 5, '吉首大学火焰山', '龚佳龙', '13367377672', '3');
INSERT INTO `orders` VALUES ('e4b6d5c56dcf454996220ec94e0546df', '2021-01-23 19:06:53', 199, 1, '启航实验室', '波波', '123', '3');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `PASSWORD` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (3, 'Freezel', '123qwe', '1433223gil@123.com', 'Y', NULL);
INSERT INTO `user` VALUES (6, 'ctrlcxk', '123123', '1079654850@qq.com', 'Y', 'de99c128ed024d3d8a416745008f1b73');
INSERT INTO `user` VALUES (7, '1433223yg', '123qwe', '1079654850@qq.com', 'Y', '12dfc95200de43f48aecca4d5f7578e8');
INSERT INTO `user` VALUES (24, '123123123', '123123123', '1079654850@qq.com', 'N', 'de6d80952fbd4d54bf648fd97ef7d6aa');
INSERT INTO `user` VALUES (28, '123qwe', '123qwe', '1079654850@qq.com', 'N', '4fb1b8850b324692b2ff7d5cb03bdc36');
INSERT INTO `user` VALUES (29, '123jsu', '123jsu', '1079654850@qq.com', 'N', 'd7fa5d597eee4af3b42a5c3e3d4f3dad');
INSERT INTO `user` VALUES (30, 'admin', 'admin', '1079654850@qq.com', 'N', NULL);
INSERT INTO `user` VALUES (31, '1332333sy', '1332333sg', '1079654850@qq.com', 'Y', '82a11e20c1c1458f9b14986f636651f9');
INSERT INTO `user` VALUES (32, 'admin123', 'admin123', '1079654850@qq.com', 'N', '5215a91133a44eb58f747193b8e390a9');
INSERT INTO `user` VALUES (33, 'adminppl', 'adminppl', '1079654850@qq.com', 'Y', 'af0006023d08488cbb9f4998c29c0f1d');
INSERT INTO `user` VALUES (35, '1079654850', '1079654850', '1079654850@qq.com', 'Y', '7999408b4e7c40429070c9b4fb1eb406');
INSERT INTO `user` VALUES (37, 'ctrl蔡徐坤', '123qwe', '10086@cnm.cn', 'Y', NULL);
INSERT INTO `user` VALUES (39, 'piplong', 'piplong', '1079654850@qq.com', 'Y', '67cb5204083f40919637f178bb0dcb11');
INSERT INTO `user` VALUES (45, 'George13', '123qwe', '1079654850@qq.com', 'Y', NULL);

-- ----------------------------
-- Table structure for warehouse
-- ----------------------------
DROP TABLE IF EXISTS `warehouse`;
CREATE TABLE `warehouse`  (
  `wid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '仓库异常 ID',
  `wdate` datetime NOT NULL COMMENT '异常发生时间 ',
  `wimage` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '异常监测图片路径',
  `status` int(11) NOT NULL COMMENT '异常状态 0：未处理 ，1：已处理',
  PRIMARY KEY (`wid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of warehouse
-- ----------------------------
INSERT INTO `warehouse` VALUES ('0107e5670c114617890e4c2dec9370aa', '2021-06-22 17:03:23', '1624698203803.jpg', 0);
INSERT INTO `warehouse` VALUES ('02d78ba67891418696e2f5deed347a0b', '2021-06-23 17:03:36', '1624698216212.jpg', 0);
INSERT INTO `warehouse` VALUES ('092869d5c40748ab857be3b9d606d146', '2021-06-24 17:00:15', '1624698014940.jpg', 0);
INSERT INTO `warehouse` VALUES ('1', '2021-06-25 20:19:49', 'abnormal001.jpg', 0);
INSERT INTO `warehouse` VALUES ('13123123', '2021-08-02 23:32:43', 'gggyyy.png', 1);
INSERT INTO `warehouse` VALUES ('1524933fac44476294a70ac3d851ff89', '2021-06-27 17:05:50', '1624698350114.jpg', 0);
INSERT INTO `warehouse` VALUES ('1670d643072a47829ba5d661506241f2', '2021-06-26 16:59:50', '1624697990330.jpg', 0);
INSERT INTO `warehouse` VALUES ('1b9112d5dc9b4003a8cde15b991a2943', '2021-06-28 17:00:06', '1624698005939.jpg', 0);
INSERT INTO `warehouse` VALUES ('1e0dcb0391ab494185448806b8d3a099', '2021-06-28 16:58:49', '1624697929380.jpg', 1);
INSERT INTO `warehouse` VALUES ('2', '2021-06-25 20:20:49', 'abnormal002.jpg', 1);
INSERT INTO `warehouse` VALUES ('204ebc7f9c4f40a5bd4c7c40a8701c67', '2021-06-27 17:05:30', '1624698330856.jpg', 0);
INSERT INTO `warehouse` VALUES ('2648b4410ddd4cd68254a05face09ec0', '2021-06-27 17:04:13', '1624698253594.jpg', 0);
INSERT INTO `warehouse` VALUES ('3', '2021-07-01 00:25:34', 'abnormal003.jpg', 1);
INSERT INTO `warehouse` VALUES ('3afef6a03f624b61ba185e5f147e78ec', '2021-07-02 15:13:46', '1625210026533.jpg', 0);
INSERT INTO `warehouse` VALUES ('3bbb5d0bd1d2499b80515ea24544ac38', '2021-07-01 08:35:46', '1625099746097.jpg', 0);
INSERT INTO `warehouse` VALUES ('3c5f5790f3a849d3851a8a394fa7173f', '2021-07-01 10:35:20', '1625106920030.jpg', 0);
INSERT INTO `warehouse` VALUES ('3f32bcea9da34e338116f11e2c65c381', '2021-06-26 17:05:55', '1624698355156.jpg', 0);
INSERT INTO `warehouse` VALUES ('41ae612226c54acc9bd6897e16d5ed62', '2021-06-26 16:59:43', '1624697983110.jpg', 0);
INSERT INTO `warehouse` VALUES ('42d0352e7103426387dd06aa57097ca0', '2021-06-26 16:59:47', '1624697986846.jpg', 0);
INSERT INTO `warehouse` VALUES ('45ac2a7482ab47d2a232509b7727ab9e', '2021-06-26 17:06:19', '1624698379707.jpg', 0);
INSERT INTO `warehouse` VALUES ('465f4c5750c14291891335164dafbdd1', '2021-07-02 14:03:59', '1625205839497.jpg', 0);
INSERT INTO `warehouse` VALUES ('5131dd97260c4f268e7f8f4310a8e9aa', '2021-06-22 16:31:48', '1624696308245.jpg', 0);
INSERT INTO `warehouse` VALUES ('5aec8a1d629b4f61967c5ddee4e58bdf', '2021-06-23 16:31:51', '1624696311415.jpg', 0);
INSERT INTO `warehouse` VALUES ('5afb9e456cb943c892be1f9f9c7595a1', '2021-06-24 17:03:47', '1624698227064.jpg', 0);
INSERT INTO `warehouse` VALUES ('5de947862d4c4ba5ba0cccae183a4e9f', '2021-06-26 17:06:27', '1624698387086.jpg', 0);
INSERT INTO `warehouse` VALUES ('68b3407d9dd442208c77a22b1534d87c', '2021-06-30 10:26:22', '1625019982221.jpg', 0);
INSERT INTO `warehouse` VALUES ('6cd07e528e124b9bb8b531b1863a00dd', '2021-06-26 17:04:19', '1624698259227.jpg', 0);
INSERT INTO `warehouse` VALUES ('70ce0bfdd9a940db8634419bc756550c', '2021-07-01 10:35:14', '1625106914767.jpg', 0);
INSERT INTO `warehouse` VALUES ('71f42e04b6fd420d891ef41ca934b765', '2021-06-26 17:04:33', '1624698273137.jpg', 0);
INSERT INTO `warehouse` VALUES ('7a9961b066834fd1a999dc067b712b4c', '2021-06-26 17:06:09', '1624698368709.jpg', 0);
INSERT INTO `warehouse` VALUES ('7b0b60a6e6904d79b01b2e98a9c4d006', '2021-06-26 16:58:56', '1624697935447.jpg', 0);
INSERT INTO `warehouse` VALUES ('7b50788d4cf449bfa94c12bd786c6b02', '2021-07-01 08:35:27', '1625099727885.jpg', 0);
INSERT INTO `warehouse` VALUES ('7c60323f437d4c3d9998c517a829c4d9', '2021-07-01 10:35:56', '1625106956577.jpg', 0);
INSERT INTO `warehouse` VALUES ('7ede9afde4dc4462af1cc6d3e9393392', '2021-06-26 17:07:06', '1624698426921.jpg', 0);
INSERT INTO `warehouse` VALUES ('80573d6076d449da9911d559c6912479', '2021-06-30 10:42:22', '1625020941889.jpg', 0);
INSERT INTO `warehouse` VALUES ('854e4a8d4c4546fa852511b962d96872', '2021-07-01 10:35:23', '1625106923629.jpg', 0);
INSERT INTO `warehouse` VALUES ('8ad6beeb98274706b7044a27e68af67e', '2021-06-26 17:07:16', '1624698436532.jpg', 1);
INSERT INTO `warehouse` VALUES ('9305ced2c83140e2ad7d43cd4c7ad65c', '2021-07-01 10:35:52', '1625106952526.jpg', 0);
INSERT INTO `warehouse` VALUES ('9df71a44bf4e4dc696fa9965394a1571', '2021-07-01 08:35:54', '1625099753924.jpg', 0);
INSERT INTO `warehouse` VALUES ('9e5acf39d14648b6abbd4996d5d8c2e7', '2021-07-01 10:36:00', '1625106959826.jpg', 0);
INSERT INTO `warehouse` VALUES ('9f211080cd3942af88c7f79ecc634450', '2021-06-26 17:04:50', '1624698290830.jpg', 0);
INSERT INTO `warehouse` VALUES ('ab2256fad45844b083dc7b2d2d14456a', '2021-06-26 17:05:27', '1624698326976.jpg', 0);
INSERT INTO `warehouse` VALUES ('ab87c1f09b7b428a81e070152696b01c', '2021-06-26 17:06:52', '1624698412371.jpg', 0);
INSERT INTO `warehouse` VALUES ('ad81969098bc40aa9337cf26f8fdb846', '2021-06-30 10:42:25', '1625020945441.jpg', 1);
INSERT INTO `warehouse` VALUES ('b2ead7a40bf94d9eabb787360d4e2008', '2021-06-26 17:01:59', '1624698119931.jpg', 0);
INSERT INTO `warehouse` VALUES ('b7156b7d422e450eaff51a0582ec7dc1', '2021-06-26 17:06:57', '1624698417372.jpg', 1);
INSERT INTO `warehouse` VALUES ('bc15b6f7be5e430cae5f6421abb55590', '2021-07-01 08:35:57', '1625099757613.jpg', 0);
INSERT INTO `warehouse` VALUES ('bd1298f8d9d04a8681c380a935ecfcf5', '2021-07-01 10:35:30', '1625106930391.jpg', 0);
INSERT INTO `warehouse` VALUES ('c15bfae697ca42da87b34074dcd9f68d', '2021-06-26 16:59:21', '1624697961616.jpg', 0);
INSERT INTO `warehouse` VALUES ('c41427f6081742198a7c330a4a8c3168', '2021-06-28 17:02:10', '1624698130359.jpg', 0);
INSERT INTO `warehouse` VALUES ('d2d446a911c2475a83615fb87d37bc71', '2021-06-25 17:02:02', '1624698122600.jpg', 0);
INSERT INTO `warehouse` VALUES ('d555c6d2c90440a1b3c1886920943bb4', '2021-06-27 17:04:23', '1624698263537.jpg', 0);
INSERT INTO `warehouse` VALUES ('d5c48d7ce9a84d78b29f1a2ab570c62c', '2021-06-27 17:07:30', '1624698449980.jpg', 1);
INSERT INTO `warehouse` VALUES ('d66db5ae2a334bf3acb88abd48be8ca2', '2021-06-30 10:27:24', '1625020043645.jpg', 1);
INSERT INTO `warehouse` VALUES ('da88bdc79aa14bce870d9a3dc6232296', '2021-06-20 17:07:02', '1624698422211.jpg', 1);
INSERT INTO `warehouse` VALUES ('de18d36472714d19a86bd07cdf94b251', '2021-07-02 14:08:55', '1625206135618.jpg', 1);
INSERT INTO `warehouse` VALUES ('e969694f92c747a1bc651500e4f5b050', '2021-06-28 17:06:38', '1624698398656.jpg', 1);
INSERT INTO `warehouse` VALUES ('ea83efc355884066b35cd224b9aba4da', '2021-06-25 16:59:54', '1624697994010.jpg', 0);
INSERT INTO `warehouse` VALUES ('ee14f6deb4cf4349b902b8fc253467db', '2021-07-01 10:35:07', '1625106907226.jpg', 0);
INSERT INTO `warehouse` VALUES ('f00807e689164b7eb77dfaae40d264fd', '2021-06-27 17:04:27', '1624698267335.jpg', 0);
INSERT INTO `warehouse` VALUES ('ffff12e1920e41298ef9fb4231232f46', '2021-06-28 17:03:28', '1624698208101.jpg', 1);

SET FOREIGN_KEY_CHECKS = 1;
