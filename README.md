# 拼优购电子商务平台

## 介绍

​	随着中国市场经济的日趋成熟，中国企业面对的竞争压力也越来越大，企业要想生存，就必须有一种高效、便于客户购物和支付的购物形式，因此网上购物这种新的商业运营模式就越来越多的商有运用到竞争中，并得到了大多数客户的认可。这种基于浏览器、服务器实现的购物方式已初具规模，一些电子商务网站的成立，改变了人们以往的购物观念。

​	如何建立企业的电子商务，如何把企业业务建立在Internet 上，就涉及到建立电子商务网站、开发符合Internet特点的有效的业务应用、管理网上的交易信息、保证网上数据安全、快速反映市场变化以及充分满足Internet 业务进一步发展的要求等。对一个商业企业来说，电子商务网站是其生存的理由和基础，同时也是企业对外展示信息、从事商务活动的窗口和界面。如何设计、建立一个经济、实用、安全、高效、稳定的网站是每个电子商务网站必须考虑的问题。

​	而要解决这些问题，就必须在提高企业内部管理效率、充分利用企业内部资源的基础上，从整体上降低成本，加快对市场的响应速度，提高服务质量，提高企业的竞争力。但是企业在利用信息化技术时，必须要考虑成本、技术难度、创造的价值等几个方面。

 

（应付学校检查的客套话，实际上拉的很，直接上页面展示图。需要项目文档的话联系 QQ：1079654850，这里就不上传了）

### 1、主界面设计

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262051898.jpg)

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262107555.jpg)

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262107284.jpg)

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262108494.jpg)

### 2、登录界面设计 

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262052018.jpg)



### 3、注册界面设计

进入注册页面，输入用户名，密码，邮箱，后获取激活码激活账号，激活码会发送到邮箱。注册需要符合注册的的正则表达

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262052920.jpg)

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262052961.jpg)

注册成功则去邮箱点击激活码激活账号

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262052964.jpg)

### 4、商品分类

鼠标点击在首页右侧全部商品或者中间商品分类，可以进入商品分类模块

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262053007.jpg)
![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262053013.jpg)

### 5、购买商品/加入购物车

进入商品介绍页面，选择需要购买的数量，点击加入购物车![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262058600.jpg)

 ![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262058686.jpg)
   点击立即购买可直接到付款页

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262058312.jpg)

 

点击确认订单即可等待发货

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262054512.jpg)



 

### 6、查看我的订单

在顶部 我的订单 里面，点击即可查看本人订单
 ![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262101014.jpg)

### 7、搜索商品

在顶部搜索栏输入需要搜索的商品信息，点击搜索即可查询商品
 ![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262101363.jpg)

### 8、管理员登录界面（管理员操作）

 ![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262102030.jpg)

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262102034.jpg)

 

### 9、分类管理（管理员操作）

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262103114.jpg) ![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262103118.jpg) ![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262103124.jpg)



### 10、用户管理（管理员操作）

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262104407.jpg) ![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262104423.jpg)![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262104422.jpg)

### 11、商品管理（管理员操作）

在管理员界面可以对商品实现增、删、改、查等功能

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262105589.jpg)



### 12、订单管理（管理员操作）

在管理员界面可以对商品实现增、删、改、查等功能，点击订单详情可查看订单项的数据

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262106436.jpg)

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262106440.jpg)

对已付款的订单可以点击“去发货”，以改变订单状态

 ![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262106888.jpg)

最后等待用户确认收货，完成订单

![img](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262106218.jpg)

## 项目演示视频

https://www.bilibili.com/video/BV1eB4y1M7iM

## 软件架构

后端：Servlet

数据存储：Redis、MySQL

前端：BootStrap

## 安装教程

1. 下载项目并解压

2. 打开 idea -> 打开新文件 -> 选择解压后的文件 。 等待项目导入

3. 数据库 & 配置文件 详见使用说明


## 使用说明

### 1.数据库配置说明

数据库在项目 根 目录下的 shopping.sql 中，建议mysql版本为 5.5 / 5.6，导入数据库即可

(项目后台：http://localhost:8080/pdd/admin)

### 详细配置说明！

#### 1. idea 导入项目，进入首页

![image-20220630153812771](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/other-img/202206301538905.png)



#### 2.进入项目结构

![image-20220630154127582](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/other-img/202206301541614.png)

#### 3. 配置 JDK

![image-20220630154105507](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/other-img/202206301541583.png)

#### 4.添加facets

![image-20220630154425510](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/Computer_network-img/202207011643771.png)



![image-20220630154458618](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/other-img/202206301544647.png)



![image-20220630154639739](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/other-img/202206301546829.png)



#### 5. 添加 Artifacts

![image-20220630154848703](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/other-img/202206301548773.png)

#### 6. 添加 tomcat

![image-20220630154742027](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/other-img/202206301547115.png)



#### 7. 配置 deployment

![image-20220630154949187](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/other-img/202206301549258.png)

可以修改一下Application context，方便访问

![image-20220630155041910](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/other-img/202206301550965.png)

点击Apply保存

正常情况下，点击右上角的运行就可以跑通的，

![image-20220630155938970](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/other-img/202206301559187.png)

如果提示

![image-20220630155235997](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/other-img/202206301552104.png)

可以在项目结构中的依赖里面

导入自己tomcat的依赖（junit是老师强迫要的，这个报错的话，鼠标放上去根据idea提示，add添加依赖就行）

![image-20220630155327085](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/other-img/202206301553123.png)

![image-20220630155402126](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/other-img/202206301554186.png)

记得导入依赖！

![image-20220626211213361](https://piplong-img.oss-cn-hangzhou.aliyuncs.com/blog-img/202206262112495.png)

## 帮助获取

部署问题 or 文档帮助 可以联系Q裙：715674363

## 感谢

黑马pink老师前端页面